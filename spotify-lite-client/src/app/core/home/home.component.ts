import { Component, OnInit } from "@angular/core";
import { Album } from "src/data-models/Album";
import { Router } from "@angular/router";
import { Apollo } from "apollo-angular";
import { getAlbumRecomendationsQuery } from "../queries.gql";

@Component({
  selector: "spotify-lite-home",
  templateUrl: "./home.component.html",
  styleUrls: ["./home.component.scss"]
})
export class HomeComponent implements OnInit {
  recommendedAlbums: Album[];
  constructor(private _router: Router, private _appollo: Apollo) {}

  ngOnInit() {
    this._appollo
      .watchQuery({
        query: getAlbumRecomendationsQuery
      })
      .valueChanges.subscribe((result: any) => {
        this.recommendedAlbums = result.data.AlbumRecomendations;
      });
  }

  onGoToAlbum(album: Album) {
    if (album && album.album_id) {
      this._router.navigate(["/core/album", album.album_id]);
    }
  }
}
