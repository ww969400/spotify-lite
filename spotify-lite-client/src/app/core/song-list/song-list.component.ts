import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter,
  ElementRef,
  Renderer2,
  ViewChild,
  ComponentRef
} from "@angular/core";
import { Song } from "src/data-models/Song";
import { Utils } from "../utils";
import { PlayerService } from "../services/player/player.service";
import {
  Option,
  OptionsMenuComponent
  OPTION_ITEM,
} from "../options-menu/options-menu.component";
import { Overlay, OverlayConfig } from "@angular/cdk/overlay";
import { ComponentPortal } from "@angular/cdk/portal";
import { AddToPlaylistComponent } from "../add-to-playlist/add-to-playlist.component";

@Component({
  selector: "spotify-lite-song-list",
  templateUrl: "./song-list.component.html",
  styleUrls: ["./song-list.component.scss"]
})
export class SongListComponent implements OnInit {
  @Input() songs: Song[];
  @Output() playSong: EventEmitter<Song> = new EventEmitter();
  @Output() removeSongFromPlaylist: EventEmitter<Song> = new EventEmitter();
  @ViewChild(OptionsMenuComponent) menu: OptionsMenuComponent;
  @Input() optionsAlowed: OPTION_ITEM[];
  currentSong: Song;
  menuOptions: Option[] = [];
  utils = Utils;

  constructor(
    private _overlay: Overlay
  ) {}

  ngOnInit() {
    this.addMenuOptions();
  }
  addMenuOptions(): any {
     if(this.optionsAlowed.includes(OPTION_ITEM.ADD_PLAYLIST)) {
       this.menuOptions.push({
         label: "Add to playlist",
         value: OPTION_ITEM.ADD_PLAYLIST
       });
     }
     if(this.optionsAlowed.includes(OPTION_ITEM.REMOVE_PLAYLIST)) {
       this.menuOptions.push({
                         label: "Remove from Playlist",
                         value: OPTION_ITEM.REMOVE_PLAYLIST
                       } );
}



  }

  showOptions(e: any, song: Song) {
    if(this.optionsAlowed.length > 0){
      this.currentSong = song;
      this.menu.showMenu(e);
    }

  }

  onMenuSelection(option: Option) {
    console.log("s o", option);
    if (option.value === OPTION_ITEM.ADD_PLAYLIST) {
      this.showAddToPlaylistPopup();
    }

    if (option.value === OPTION_ITEM.REMOVE_PLAYLIST) {
      this.removeSongFromPlaylist.emit(this.currentSong);
    }

  }

  showAddToPlaylistPopup(): any {
    const positionStrategy = this._overlay
      .position()
      .global()
      .centerHorizontally()
      .top("200px");

    const overlayConfig = new OverlayConfig({
      hasBackdrop: true,
      backdropClass: "spotify-lite-playlist-overlay-backdrop",
      panelClass: "spotify-lite-playlist-overlay-pannel",
      scrollStrategy: this._overlay.scrollStrategies.block(),
      positionStrategy
    });
    const overlayRef = this._overlay.create(overlayConfig);
    const createPlaylistPortal = new ComponentPortal(AddToPlaylistComponent);
    const compRef: ComponentRef<AddToPlaylistComponent> = overlayRef.attach(
      createPlaylistPortal
    );
    compRef.instance.songToAdd = this.currentSong;
    compRef.instance.closePopup.subscribe(() => {
      overlayRef.detach();
    });
  }
}
