import { Component, OnInit, ComponentRef } from "@angular/core";
import { Apollo } from "apollo-angular";
import {
  getPlaylistById,
  delPlaylistById,
  deleteSongFromPlaylist
} from "../queries.gql";
import { ActivatedRoute, Router } from "@angular/router";
import { Overlay, OverlayConfig } from "@angular/cdk/overlay";
import { ComponentPortal } from "@angular/cdk/portal";
import { ConfirmDeleteComponent } from "../confirm-delete/confirm-delete.component";
import { OPTION_ITEM } from "../options-menu/options-menu.component";
import { Song } from "src/data-models/Song";
import { PlayerService } from "../services/player/player.service";

@Component({
  selector: "spotify-lite-playlist",
  templateUrl: "./playlist.component.html",
  styleUrls: ["./playlist.component.scss"]
})
export class PlaylistComponent implements OnInit {
  playlist: any;
  optionsAlowed: OPTION_ITEM[] = [OPTION_ITEM.REMOVE_PLAYLIST];

  constructor(
    private _apollo: Apollo,
    private _route: ActivatedRoute,
    private _overlay: Overlay,
    private _router: Router,
    private _playerSrv: PlayerService
  ) {}

  ngOnInit() {
    this.getPlaylist();
  }
  getPlaylist(): any {
    const playlistId = this._route.snapshot.paramMap.get("id");

    this._apollo
      .watchQuery({
        query: getPlaylistById,
        variables: {
          playlistId
        },
        fetchPolicy: "no-cache"
      })
      .valueChanges.subscribe((result: any) => {
        console.log(result, "results");
        this.playlist = result.data.getPlaylistById;
      });
  }

  deletePlaylist(playlist: any) {
    if (this._playerSrv.isPlaying) {
      this._playerSrv.stop();
    }
    const positionStrategy = this._overlay
      .position()
      .global()
      .centerHorizontally()
      .centerVertically();

    const overlayConfig = new OverlayConfig({
      hasBackdrop: true,
      backdropClass: "spotify-lite-playlist-overlay-backdrop",
      panelClass: "spotify-lite-playlist-overlay-pannel",
      scrollStrategy: this._overlay.scrollStrategies.block(),
      positionStrategy
    });
    const overlayRef = this._overlay.create(overlayConfig);
    const createPlaylistPortal = new ComponentPortal(ConfirmDeleteComponent);
    const compRef: ComponentRef<ConfirmDeleteComponent> = overlayRef.attach(
      createPlaylistPortal
    );
    compRef.instance.closePopup.subscribe(shouldDelete => {
      overlayRef.detach();
      if (shouldDelete) {
        this._apollo
          .mutate({
            mutation: delPlaylistById,
            variables: {
              playlistId: playlist.playlist_id
            }
          })
          .subscribe(
            ({ data }) => {
              console.log("got data", data);
              this._router.navigate(["/core/yourMusic"]);
            },
            error => {
              console.log("there was an error sending the query", error);
            }
          );
      }
    });
  }

  onRemoveSongFromPlaylist(song: Song) {
    this._apollo
      .mutate({
        mutation: deleteSongFromPlaylist,
        variables: {
          playlistId: this.playlist.playlist_id,
          trackId: song.track_id
        }
      })
      .subscribe(
        ({ data }) => {
          console.log("got data", data);
          this.getPlaylist();
        },
        error => {
          console.log("there was an error sending the query", error);
        }
      );
  }
  onTrackClick(song: Song) {
    console.log(song);
    this._playerSrv.currentSong = song;
    this._playerSrv.setTrack(song, this.playlist.tracks, true);
  }
}
