import { Howl, Howler } from "howler";
import { Song } from "src/data-models/Song";
import { environment } from "src/environments/environment";
export class Utils {
  static setTrackDuration(songs: Song[]) {
    songs.forEach((song: Song) => {
      new Howl({
        src: [`${environment.s3Base}${song.track_path}`],
        onload: function() {
          song.track_duration = Utils.msToTime(this.duration());
        }
      });
    });
  }
  static msToTime(s) {
    const time = Math.round(s);
    const mins = Math.floor(time / 60);
    const secs = time - mins * 60;
    const extraZero = secs < 10 ? "0" : "";

    return `${mins}:${extraZero}${secs}`;
  }

  static fancyTimeFormat(time) {
    // Hours, minutes and seconds
    var hrs = ~~(time / 3600);
    var mins = ~~((time % 3600) / 60);
    var secs = ~~time % 60;

    // Output like "1:01" or "4:03:59" or "123:03:59"
    var ret = "";

    if (hrs > 0) {
      ret += "" + hrs + ":" + (mins < 10 ? "0" : "");
    }

    ret += "" + mins + ":" + (secs < 10 ? "0" : "");
    ret += "" + secs;
    return ret;
  }
}
