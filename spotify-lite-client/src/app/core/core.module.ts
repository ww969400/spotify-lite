import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";
import { CoreComponent } from "./core/core.component";
import { HomeComponent } from "./home/home.component";
import { NowPlayingBarComponent } from "./now-playing-bar/now-playing-bar.component";
import { NavigationComponent } from "./navigation/navigation.component";
import { MusicService } from "./services/music/music.service";
import { HttpClientModule } from "@angular/common/http";
import { AlbumComponent } from "./album/album.component";
import { PlayerService } from "./services/player/player.service";
import { ArtistComponent } from "./artist/artist.component";
import { SongListComponent } from "./song-list/song-list.component";
import { AlbumGridComponent } from "./album-grid/album-grid.component";
import { SearchComponent } from "./search/search.component";
import { SettingsComponent } from "./settings/settings.component";
import { UserDetailsComponent } from "./user-details/user-details.component";
import { YourMusicComponent } from "./your-music/your-music.component";
import { OverlayModule } from "@angular/cdk/overlay";
import { CreatePlaylistComponent } from "./create-playlist/create-playlist.component";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { PlaylistComponent } from "./playlist/playlist.component";
import { ConfirmDeleteComponent } from "./confirm-delete/confirm-delete.component";
import { OptionsMenuComponent } from "./options-menu/options-menu.component";
import { AddToPlaylistComponent } from "./add-to-playlist/add-to-playlist.component";
import { PlaylistGridComponent } from './playlist-grid/playlist-grid.component';
import { AlertComponent } from './alert/alert.component';

const routes: Routes = [
  {
    path: "",
    component: CoreComponent,
    children: [
      { path: "", redirectTo: "home", pathMatch: "full" },
      {
        path: "home",
        component: HomeComponent
      },
      {
        path: "album/:id",
        component: AlbumComponent
      },
      {
        path: "artist/:id",
        component: ArtistComponent
      },
      {
        path: "playlist/:id",
        component: PlaylistComponent
      },
      {
        path: "search",
        component: SearchComponent
      },
      {
        path: "yourMusic",
        component: YourMusicComponent
      },
      {
        path: "settings",
        component: SettingsComponent
      },
      {
        path: "user",
        component: UserDetailsComponent
      }
    ]
  }
];
@NgModule({
  declarations: [
    CoreComponent,
    HomeComponent,
    NowPlayingBarComponent,
    NavigationComponent,
    AlbumComponent,
    ArtistComponent,
    SongListComponent,
    AlbumGridComponent,
    SearchComponent,
    SettingsComponent,
    UserDetailsComponent,
    YourMusicComponent,
    CreatePlaylistComponent,
    PlaylistComponent,
    ConfirmDeleteComponent,
    OptionsMenuComponent,
    AddToPlaylistComponent,
    PlaylistGridComponent,
    AlertComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    HttpClientModule,
    OverlayModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [MusicService, PlayerService],
  entryComponents: [
    CreatePlaylistComponent,
    ConfirmDeleteComponent,
    AddToPlaylistComponent
  ]
})
export class CoreModule {}
