import { Injectable } from "@angular/core";
import { Song } from "src/data-models/Song";
import { Howl, Howler } from "howler";
import { environment } from "src/environments/environment";
import { Album } from "src/data-models/Album";
import { Subject } from "rxjs";
import { NowPlayingGroup } from "src/data-models/NowPlayingGroup";
import { getAlbumById, updateTrackPlays } from "../../queries.gql";
import { Apollo } from "apollo-angular";

@Injectable({
  providedIn: "root"
})
export class PlayerService {
  private _currentTrack: Song;
  private _currentAlbum: Album;
  private _audio: any;
  private _muted: boolean;
  private _volumeSubject = new Subject<any>();
  volume$ = this._volumeSubject.asObservable();
  nowPlayingGroup: NowPlayingGroup;
  constructor(private _apollo: Apollo) {}

  setTrack(track: Song, newPlaylist: Song[] | Album, play = false) {
    this.currentSong = track;
    this.nowPlayingGroup = new NowPlayingGroup(newPlaylist, track);
    //  Utils.setTrackDuration(this.currentPlaylist);
    this._setCurrentAlbum();
    if (play) {
      this.playSong();
    }
  }

  private _setCurrentAlbum() {
    if (
      this._currentAlbum &&
      this._currentAlbum.album_id === this._currentTrack.album_id
    ) {
      return false;
    }

    if (this._currentTrack) {
      this._apollo
        .watchQuery({
          query: getAlbumById,
          variables: {
            _albumId: this._currentTrack.album_id
          }
        })
        .valueChanges.subscribe((result: any) => {
          this._currentAlbum = result.data.getAlbumById;
        });
    }
  }
  private _handleOnplay(soundId: number) {
    if (this._currentTrack && this._currentTrack.soundId === soundId) {
      //resuming a tack
    } else {
      // starting a track

      this._currentTrack.soundId = soundId;
      console.log(this._currentTrack);
      //  this.updateTrackPlaysForCurrentTrack();
    }
  }

  updateTrackPlaysForCurrentTrack() {
    this._apollo
      .mutate({
        mutation: updateTrackPlays,
        variables: {
          trackId: this.currentTrack.track_id
        }
      })
      .subscribe(
        ({ data }) => {
          console.log("got data", data);
        },
        error => {
          console.log("there was an error sending the query", error);
        }
      );
  }

  set currentSong(song: Song) {
    if (song) {
      this._currentTrack = song;
      this._setCurrentAlbum();
    }
    if (this.isPlaying) {
      this._audio.stop();
    }
    const $this = this;
    this._audio = new Howl({
      src: [`${environment.s3Base}${$this._currentTrack.track_path}`],
      onplay: this._handleOnplay.bind($this),
      onload: () => {
        this._volumeSubject.next(this._audio.volume() * 100);
      },
      onvolume: e => {
        this._volumeSubject.next(this._audio.volume() * 100);
      },
      onend: function() {
        $this.nextTrack();
      }
    });
  }
  playSong() {
    this._audio.play();
  }
  currentTime() {
    return this._audio ? this._audio.seek() : 0;
  }
  get isPlaying() {
    return this._audio && this._audio.playing();
  }

  get currentTrack() {
    return this._currentTrack;
  }

  get currentAlbum() {
    return this._currentAlbum;
  }

  pauseSong() {
    this._audio.pause();
  }

  stop(): any {
    this._audio.stop();
  }
  set mute(mute: boolean) {
    this._muted = mute;
    Howler.mute(this._muted);
  }

  get mute() {
    return this._muted;
  }

  getDuration() {
    return this._audio.duration();
  }

  setSeek(s) {
    this._audio.seek(s);
  }
  set volume(v) {
    Howler.volume(v);
  }

  nextTrack() {
    this.currentSong = this.nowPlayingGroup.next();
    this.playSong();
  }
  prevTrack() {
    this.currentSong = this.nowPlayingGroup.prev();
    this.playSong();
  }
  toggleRepeat() {
    this.nowPlayingGroup.repeat = !this.nowPlayingGroup.repeat;
  }
  toggleShuffle() {
    this.nowPlayingGroup.shuffle = !this.nowPlayingGroup.shuffle;
  }
}
