import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { environment } from "src/environments/environment";
import { map } from "rxjs/operators";
import { Observable } from "rxjs";
import { Album } from "src/data-models/Album";
import { Song } from "src/data-models/Song";
@Injectable({
  providedIn: "root"
})
export class MusicService {
  constructor(private _http: HttpClient) {}

  getAlbums(): Observable<Album[]> {
    return this._http
      .get(`${environment.serviceBase}/albums/list`)
      .pipe(map(res => res["Items"]));
  }

  getAlbumById(id: string): Observable<Album> {
    return this._http
      .get(`${environment.serviceBase}/albums/getOne/${id}`)
      .pipe(map(res => res["Item"]));
  }

  getRandomPlaylist(): Observable<Song[]> {
    return this._http.get(`${environment.serviceBase}/albums/list`).pipe(
      map((res: any) => {
        return res["Items"].map(album => album.songs[0]);
      })
    );
  }

  updatePlays(song: Song) {
    return this._http.post(`${environment.serviceBase}/albums/upd`, song);
  }
}
