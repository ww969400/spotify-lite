import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { fromEvent } from "rxjs";
import { tap, map, debounceTime } from "rxjs/operators";

@Component({
  selector: "spotify-lite-search",
  templateUrl: "./search.component.html",
  styleUrls: ["./search.component.scss"]
})
export class SearchComponent implements OnInit {
  @ViewChild("searchInput") searchInput: ElementRef;

  constructor() {}

  ngOnInit() {
    let buttonStream$ = fromEvent(this.searchInput.nativeElement, "keyup")
      .pipe(
        map((e: any) => e.target.value),
        debounceTime(2000),
        tap(console.log)
      )
      .subscribe(this.handleSearch);
  }

  handleSearch(value: string) {}
}
