//@angular
import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { Routes, RouterModule } from "@angular/router";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";

//angularfire2
import { AngularFireModule } from "angularfire2";
import { AngularFireAuthModule } from "angularfire2/auth";
import { AngularFireDatabaseModule } from "angularfire2/database";

//@rxjs

//module
import { LoginComponent } from "./login/login.component";
import { RegisterComponent } from "./register/register.component";

//misc
import { environment } from "src/environments/environment";
import { AuthenticationComponent } from "./authentication/authentication.component";
import { AuthenticationService } from "./services/auth/authentication.service";
import { AuthGuardService } from "./services/authGaurd/auth.gaurd";

const routes: Routes = [
  {
    path: "",
    component: AuthenticationComponent,
    children: [
      { path: "", redirectTo: "login", pathMatch: "full" },
      {
        path: "login",
        component: LoginComponent
      },
      {
        path: "register",
        component: RegisterComponent
      }
    ]
  }
];

@NgModule({
  declarations: [RegisterComponent, LoginComponent, AuthenticationComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule
  ],
  providers: [AuthenticationService, AuthGuardService]
})
export class AuthenticationModule {}
