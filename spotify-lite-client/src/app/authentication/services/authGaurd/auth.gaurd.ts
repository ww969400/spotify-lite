import { Injectable } from "@angular/core";
import { Router, CanActivate } from "@angular/router";
import { AuthenticationService } from "../auth/authentication.service";
import { Observable, of } from "rxjs";
import { switchMap } from "rxjs/operators";

@Injectable()
export class AuthGuardService implements CanActivate {
  constructor(public _authSrv: AuthenticationService, public router: Router) {}

  canActivate(): Observable<boolean> {
    return this._authSrv.isAuthenticated().pipe(
      switchMap(isAuthenticated => {
        if (!isAuthenticated) {
          this.router.navigate(["login"]);
          return of(false);
        }
        return of(true);
      })
    );
  }
}
