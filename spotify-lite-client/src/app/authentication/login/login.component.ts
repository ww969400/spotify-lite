//@angular
import { Component, OnInit } from "@angular/core";
import {
  FormGroup,
  FormControl,
  FormBuilder,
  Validators
} from "@angular/forms";
import { Router } from "@angular/router";
import { AuthenticationService } from "../services/auth/authentication.service";

@Component({
  selector: "spotify-lite-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"]
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;

  constructor(
    private _fb: FormBuilder,
    private _authService: AuthenticationService,
    private _router: Router
  ) {}

  ngOnInit() {
    this.initializeLoginForm();
  }

  // convenience getter for easy access to form fields
  get fields() {
    return this.loginForm.controls;
  }

  initializeLoginForm() {
    this.loginForm = this._fb.group({
      email: new FormControl("", [Validators.required, Validators.email]),
      password: new FormControl("", [
        Validators.required,
        ,
        Validators.minLength(6)
      ])
    });
  }

  onSubmit() {
    // TODO: Use EventEmitter with form value
    console.warn(this.loginForm.value);

    this._authService.doLogin(this.loginForm.value).then(
      res => {
        console.log(res);
        this._router.navigate(["/", "core"]);
      },
      err => {
        console.log(err);
      }
    );
  }
}
