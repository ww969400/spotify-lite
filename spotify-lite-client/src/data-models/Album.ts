export class Album {
  album_id: string;
  album_name: string;
  genre_id: string;
  artist_id: string;
  album_artwork: string;
  tracks: any[];
}
