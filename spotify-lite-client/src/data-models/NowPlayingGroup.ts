import { Song } from "./Song";
import { Album } from "./Album";
import { Playlist } from "./Playlist";
declare const _: any;
export class NowPlayingGroup {
  private _repeat: any;
  private _shuffle: any;
  private _shuffledSongs: any;
  private _songGroup: any;
  constructor(
    private songs: Song[] | Album | Playlist,
    private currentSong?: Song
  ) {
    if (!currentSong) {
      currentSong = songs[0];
    }
    this._songGroup = songs;
  }

  next(): any {
    if (this._repeat) {
      return this.currentSong;
    }

    this.currentSong =
      this._songGroup[
        _.findIndex(this._songGroup, ["track_id", this.currentSong.track_id]) +
          1
      ] || this._songGroup[0];

    return this.currentSong;
  }

  prev(): Song {
    if (this._repeat) {
      return this.currentSong;
    }
    const indOfCurrent = _.findIndex(this._songGroup, [
      "track_id",
      this.currentSong.track_id
    ]);
    this.currentSong =
      indOfCurrent == 0 ? this.currentSong : this._songGroup[indOfCurrent - 1];

    return this.currentSong;
  }

  set repeat(val: boolean) {
    this._repeat = val;
  }

  get repeat() {
    return this._repeat;
  }

  set shuffle(val: boolean) {
    if (val) {
      this._shuffledSongs = _.shuffle(this.songs);
      this._songGroup = this._shuffledSongs;
    } else {
      this._songGroup = this.songs;
    }
    this._shuffle = val;
  }

  get shuffle() {
    return this._shuffle;
  }
}
