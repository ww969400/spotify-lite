import { Song } from "./Song";

export class Track extends Song {
  soundId?: number; // howler sound id
  duration;
}
