const connection = require("./knexfile");
const _ = require("lodash");

const knex = require("knex")(
  process.env.NODE_ENV === "production"
    ? connection.production
    : connection.development
);

// eslint-disable-next-line import/prefer-default-export
export const resolvers = {
  Mutation: {
    deleteSongFromPlaylist: (root, args) =>
      knex("PlaylistSongs")
        .where("PlaylistSongs.playlist_id", args.playlistId)
        .andWhere("PlaylistSongs.track_id", args.trackId)
        .del()
        .then(delCt => delCt),
    delPlaylistById: (root, args) =>
      knex("Playlists")
        .where("Playlists.playlist_id", args.playlistId)
        .del()
        .then(delCt => delCt),
    createPlaylist: (root, args) => {
      const playlist = {
        playlist_name: args.playlistName,
        playlist_owner: args.playlistOwner
      };
      return knex("Playlists")
        .insert(playlist)
        .then(id => {
          return knex
            .select("*")
            .from("Playlists")
            .where("Playlists.playlist_id", id[0])
            .then(playlists => {
              if (!playlists) {
                throw new Error("No Playlists found");
              }
              return playlists[0];
            });
        });
    },
    updateTrackPlays: (root, args) =>
      knex("Tracks")
        .where("Tracks.track_id", args.trackId)
        .increment("track_plays", 1)
        .then(() => {
          return knex
            .select("*")
            .from("Tracks")
            .where("Tracks.track_id", args.trackId)
            .then(tracks => {
              if (!tracks) {
                throw new Error("No Tracks found");
              }
              return tracks[0];
            });
        }),

    addToPlaylist: (root, args) => {
      const playlistSong = {
        playlist_id: args.playlistId,
        track_id: args.trackId
      };

      return knex("PlaylistSongs")
        .insert(playlistSong)
        .then(res => res);
    }
  },
  Query: {
    getArtistById: (root, args) =>
      knex
        .select("*")
        .from("Artists")
        .where("Artists.artist_id", args.artistId)
        .then(artists => {
          if (!artists) {
            throw new Error("No artists found");
          }
          const artist = artists[0];

          return knex
            .select("*")
            .from("Tracks")
            .where("Tracks.artist_id", artist.artist_id)
            .orderBy("track_plays", "desc")
            .limit(5)
            .then(tracks => {
              if (!tracks) {
                throw new Error("No tracks found");
              }

              artist.topTracks = tracks;

              return knex
                .select("*")
                .from("Albums")
                .leftJoin("Artists", "Albums.artist_id", "Artists.artist_id")
                .where("Albums.artist_id", artist.artist_id)
                .then(albums => {
                  artist.albums = albums.filter(a => a.album_type === "Full");
                  artist.eps = albums.filter(a => a.album_type === "Ep");
                  return artist;
                });
            });
        }),
    getPlaylistByUsername: (root, args) =>
      knex
        .select("*")
        .from("Playlists")
        .where("Playlists.playlist_owner", args.userName)
        .then(playlists => {
          if (!playlists) {
            throw new Error("No playlists found");
          }
          return playlists;
        }),
    getPlaylistById: (root, args) =>
      knex
        .select("*")
        .from("Playlists")
        .where("Playlists.playlist_id", args.playlistId)
        .then(playlists => {
          const playlist = playlists[0];
          return knex
            .select("*")
            .from("PlaylistSongs")
            .leftJoin("Tracks", "PlaylistSongs.track_id", "Tracks.track_id")
            .leftJoin("Artists", "Tracks.artist_id", "Artists.artist_id")
            .leftJoin("Albums", "Albums.album_id", "Tracks.album_id")
            .where("PlaylistSongs.playlist_id", playlist.playlist_id)
            .then(songs => {
              playlist.tracks = songs || [];
              return playlist;
            });
        }),
    getAlbumById: (root, args) =>
      knex
        .select("*")
        .from("Albums")
        .leftJoin("Genres", "Albums.genre_id", "Genres.genre_id")
        .leftJoin("Artists", "Albums.artist_id", "Artists.artist_id")
        .where("Albums.album_id", args.albumId)
        .then(albums => {
          if (!albums) {
            throw new Error("No album found");
          }
          const album = albums[0];
          return knex
            .select("*")
            .from("Tracks")
            .leftJoin("Artists", "Tracks.artist_id", "Artists.artist_id")
            .leftJoin("Albums", "Tracks.album_id", "Albums.album_id")
            .where("Tracks.album_id", album.album_id)
            .then(tracks => {
              if (!tracks) {
                throw new Error("No tracks found");
              }
              album.tracks = tracks;
              return album;
            });
        }),
    AlbumRecomendations: (root, args) =>
      knex
        .select("*")
        .from("Albums")
        .leftJoin("Artists", "Albums.artist_id", "Artists.artist_id")
        .then(albums => {
          if (!albums) {
            throw new Error("No albums found");
          }
          return _.sampleSize(albums, 8);
        }),
    Albums: (root, args) =>
      knex
        .select("*")
        .from("Albums")
        .leftJoin("Genres", "Albums.genre_id", "Genres.genre_id")
        .leftJoin("Artists", "Albums.artist_id", "Artists.artist_id")
        .then(albums => {
          if (!albums) {
            throw new Error("No albums found");
          }
          return albums;
        }),

    Genres: (root, args) =>
      knex
        .select("*")
        .from("Genres")
        .then(genres => {
          if (!genres) {
            throw new Error("No genres found");
          }
          return genres;
        }),
    Artists: (root, args) =>
      knex
        .select("*")
        .from("Artists")
        .then(artists => {
          if (!artists) {
            throw new Error("No Artists found");
          }
          return artists;
        }),
    Playlists: (root, args) =>
      knex
        .select("*")
        .from("Playlists")
        .then(playlists => {
          if (!playlists) {
            throw new Error("No Playlists found");
          }
          return playlists;
        }),
    Tracks: (root, args) =>
      knex
        .select("*")
        .from("Tracks")
        .then(tracks => {
          console.log("selected", tracks);
          if (!tracks) {
            throw new Error("No Artists found");
          }
          return tracks;
        })
  }
};
