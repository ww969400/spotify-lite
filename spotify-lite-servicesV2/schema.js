const schema = `
type Mutation {
    createAlbum(
        title: String!
        genre: Int!
        artists: [Int!]!
        songs: [Int!]!
        artworkPath: String!
        created_at: String!
    ): Album!

    updateTrackPlays (
      trackId: String!
    ):Track!

    createPlaylist(
      playlistName: String!
      playlistOwner: String!
    ): Playlist

    delPlaylistById(
      playlistId: Int!
    ): Int!

    addToPlaylist(
      playlistId: Int!
      trackId: String!
    ): [Int]!
    deleteSongFromPlaylist(
      playlistId: Int!
      trackId: String!
    ): Int!
}



enum AlbumType {
  Full
  Ep
}

type Query {
    AlbumRecomendations: [Album]!
    getAlbumById(albumId: String!): Album!
    getArtistById(artistId: String!): Artist!
    getPlaylistByUsername(userName: String!): [Playlist!]!
    getPlaylistById(playlistId: Int!): Playlist!
    Albums: [Album]!
    Genres: [Genre]!
    Artists:[Artist]!
    Tracks: [Track]!
    Playlists: [Playlist]!

}

type Genre {
  genre_id: String!
  genre_name: String!
  created_at: String!
}

type Artist {
  artist_id: String!
  artist_name: String!
  topTracks: [Track]!
  albums: [Album]!
  eps: [Album]!
  created_at: String!
}

type  Track {
  track_id: String!
  track_name: String!
  album_id: String!
  genre_id: String!
  track_plays: Int!
  track_path: String!
  artist_id: String!
  artist_name: String!
  album_artwork: String!
  track_duration: Float!
  is_single: Boolean!
  created_at: String!
}

type Album {
  album_id: String!
  album_type: AlbumType!
  album_name: String!
  genre_id: String!
  artist_id: String!
  album_artwork: String!
  created_at: String!
  genre_name: String!
  artist_name: String!
  tracks: [Track]!
}

type Playlist {
  playlist_id: Int!
  playlist_name: String!
  playlist_owner: String!
  created_at: String!
  tracks: [Track]!

}
schema {
    query: Query
    mutation: Mutation
}`;

// eslint-disable-next-line import/prefer-default-export
export { schema };
