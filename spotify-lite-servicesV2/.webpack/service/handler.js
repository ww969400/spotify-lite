(function(e, a) { for(var i in a) e[i] = a[i]; }(exports, /******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, {
/******/ 				configurable: false,
/******/ 				enumerable: true,
/******/ 				get: getter
/******/ 			});
/******/ 		}
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 1);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ (function(module, exports) {

module.exports = require("graphql");

/***/ }),
/* 1 */
/***/ (function(module, exports, __webpack_require__) {

/*** IMPORTS FROM imports-loader ***/
var graphql = __webpack_require__(0);

"use strict";

var _apolloServerLambda = __webpack_require__(2);

var _graphqlPlaygroundMiddlewareLambda = __webpack_require__(3);

var _graphqlPlaygroundMiddlewareLambda2 = _interopRequireDefault(_graphqlPlaygroundMiddlewareLambda);

var _graphqlTools = __webpack_require__(4);

var _schema = __webpack_require__(5);

var _resolvers = __webpack_require__(6);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

const myGraphQLSchema = (0, _graphqlTools.makeExecutableSchema)({
  typeDefs: _schema.schema,
  resolvers: _resolvers.resolvers,
  logger: console
}); //import "babel-polyfill";


exports.graphqlHandler = function graphqlHandler(event, context, callback) {
  context.callbackWaitsForEmptyEventLoop = false;

  function callbackFilter(error, output) {
    if (!output.headers) {
      output.headers = {};
    }
    // eslint-disable-next-line no-param-reassign
    output.headers["Access-Control-Allow-Origin"] = "*";
    callback(error, output);
  }

  const handler = (0, _apolloServerLambda.graphqlLambda)({ schema: myGraphQLSchema, tracing: true });
  return handler(event, context, callbackFilter);
};

// for local endpointURL is /graphql and for prod it is /stage/graphql
exports.playgroundHandler = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;
  return (0, _graphqlPlaygroundMiddlewareLambda2.default)({
    endpoint: process.env.REACT_APP_GRAPHQL_ENDPOINT ? process.env.REACT_APP_GRAPHQL_ENDPOINT : "/production/graphql"
  })(event, context, callback);
};

exports.graphiqlHandler = (event, context, callback) => {
  context.callbackWaitsForEmptyEventLoop = false;
  return (0, _apolloServerLambda.graphiqlLambda)({
    endpointURL: process.env.REACT_APP_GRAPHQL_ENDPOINT ? process.env.REACT_APP_GRAPHQL_ENDPOINT : "/production/graphql"
  })(event, context, callback);
};


/***/ }),
/* 2 */
/***/ (function(module, exports) {

module.exports = require("apollo-server-lambda");

/***/ }),
/* 3 */
/***/ (function(module, exports) {

module.exports = require("graphql-playground-middleware-lambda");

/***/ }),
/* 4 */
/***/ (function(module, exports) {

module.exports = require("graphql-tools");

/***/ }),
/* 5 */
/***/ (function(module, exports, __webpack_require__) {

/*** IMPORTS FROM imports-loader ***/
var graphql = __webpack_require__(0);

"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
const schema = `
type Mutation {
    createAlbum(
        title: String!
        genre: Int!
        artists: [Int!]!
        songs: [Int!]!
        artworkPath: String!
        created_at: String!
    ): Album!

    updateTrackPlays (
      trackId: String!
    ):Track!

    createPlaylist(
      playlistName: String!
      playlistOwner: String!
    ): Playlist

    delPlaylistById(
      playlistId: Int!
    ): Int!

    addToPlaylist(
      playlistId: Int!
      trackId: String!
    ): [Int]!
    deleteSongFromPlaylist(
      playlistId: Int!
      trackId: String!
    ): Int!
}



enum AlbumType {
  Full
  Ep
}

type Query {
    AlbumRecomendations: [Album]!
    getAlbumById(albumId: String!): Album!
    getArtistById(artistId: String!): Artist!
    getPlaylistByUsername(userName: String!): [Playlist!]!
    getPlaylistById(playlistId: Int!): Playlist!
    Albums: [Album]!
    Genres: [Genre]!
    Artists:[Artist]!
    Tracks: [Track]!
    Playlists: [Playlist]!

}

type Genre {
  genre_id: String!
  genre_name: String!
  created_at: String!
}

type Artist {
  artist_id: String!
  artist_name: String!
  topTracks: [Track]!
  albums: [Album]!
  eps: [Album]!
  created_at: String!
}

type  Track {
  track_id: String!
  track_name: String!
  album_id: String!
  genre_id: String!
  track_plays: Int!
  track_path: String!
  artist_id: String!
  artist_name: String!
  album_artwork: String!
  track_duration: Float!
  is_single: Boolean!
  created_at: String!
}

type Album {
  album_id: String!
  album_type: AlbumType!
  album_name: String!
  genre_id: String!
  artist_id: String!
  album_artwork: String!
  created_at: String!
  genre_name: String!
  artist_name: String!
  tracks: [Track]!
}

type Playlist {
  playlist_id: Int!
  playlist_name: String!
  playlist_owner: String!
  created_at: String!
  tracks: [Track]!

}
schema {
    query: Query
    mutation: Mutation
}`;

// eslint-disable-next-line import/prefer-default-export
exports.schema = schema;


/***/ }),
/* 6 */
/***/ (function(module, exports, __webpack_require__) {

/*** IMPORTS FROM imports-loader ***/
var graphql = __webpack_require__(0);

"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
const connection = __webpack_require__(7);
const _ = __webpack_require__(10);

const knex = __webpack_require__(11)(process.env.NODE_ENV === "production" ? connection.production : connection.development);

// eslint-disable-next-line import/prefer-default-export
const resolvers = exports.resolvers = {
  Mutation: {
    deleteSongFromPlaylist: (root, args) => knex("PlaylistSongs").where("PlaylistSongs.playlist_id", args.playlistId).andWhere("PlaylistSongs.track_id", args.trackId).del().then(delCt => delCt),
    delPlaylistById: (root, args) => knex("Playlists").where("Playlists.playlist_id", args.playlistId).del().then(delCt => delCt),
    createPlaylist: (root, args) => {
      const playlist = {
        playlist_name: args.playlistName,
        playlist_owner: args.playlistOwner
      };
      return knex("Playlists").insert(playlist).then(id => {
        return knex.select("*").from("Playlists").where("Playlists.playlist_id", id[0]).then(playlists => {
          if (!playlists) {
            throw new Error("No Playlists found");
          }
          return playlists[0];
        });
      });
    },
    updateTrackPlays: (root, args) => knex("Tracks").where("Tracks.track_id", args.trackId).increment("track_plays", 1).then(() => {
      return knex.select("*").from("Tracks").where("Tracks.track_id", args.trackId).then(tracks => {
        if (!tracks) {
          throw new Error("No Tracks found");
        }
        return tracks[0];
      });
    }),

    addToPlaylist: (root, args) => {
      const playlistSong = {
        playlist_id: args.playlistId,
        track_id: args.trackId
      };

      return knex("PlaylistSongs").insert(playlistSong).then(res => res);
    }
  },
  Query: {
    getArtistById: (root, args) => knex.select("*").from("Artists").where("Artists.artist_id", args.artistId).then(artists => {
      if (!artists) {
        throw new Error("No artists found");
      }
      const artist = artists[0];

      return knex.select("*").from("Tracks").where("Tracks.artist_id", artist.artist_id).orderBy("track_plays", "desc").limit(5).then(tracks => {
        if (!tracks) {
          throw new Error("No tracks found");
        }

        artist.topTracks = tracks;

        return knex.select("*").from("Albums").leftJoin("Artists", "Albums.artist_id", "Artists.artist_id").where("Albums.artist_id", artist.artist_id).then(albums => {
          artist.albums = albums.filter(a => a.album_type === "Full");
          artist.eps = albums.filter(a => a.album_type === "Ep");
          return artist;
        });
      });
    }),
    getPlaylistByUsername: (root, args) => knex.select("*").from("Playlists").where("Playlists.playlist_owner", args.userName).then(playlists => {
      if (!playlists) {
        throw new Error("No playlists found");
      }
      return playlists;
    }),
    getPlaylistById: (root, args) => knex.select("*").from("Playlists").where("Playlists.playlist_id", args.playlistId).then(playlists => {
      const playlist = playlists[0];
      return knex.select("*").from("PlaylistSongs").leftJoin("Tracks", "PlaylistSongs.track_id", "Tracks.track_id").leftJoin("Artists", "Tracks.artist_id", "Artists.artist_id").leftJoin("Albums", "Albums.album_id", "Tracks.album_id").where("PlaylistSongs.playlist_id", playlist.playlist_id).then(songs => {
        playlist.tracks = songs || [];
        return playlist;
      });
    }),
    getAlbumById: (root, args) => knex.select("*").from("Albums").leftJoin("Genres", "Albums.genre_id", "Genres.genre_id").leftJoin("Artists", "Albums.artist_id", "Artists.artist_id").where("Albums.album_id", args.albumId).then(albums => {
      if (!albums) {
        throw new Error("No album found");
      }
      const album = albums[0];
      return knex.select("*").from("Tracks").leftJoin("Artists", "Tracks.artist_id", "Artists.artist_id").leftJoin("Albums", "Tracks.album_id", "Albums.album_id").where("Tracks.album_id", album.album_id).then(tracks => {
        if (!tracks) {
          throw new Error("No tracks found");
        }
        album.tracks = tracks;
        return album;
      });
    }),
    AlbumRecomendations: (root, args) => knex.select("*").from("Albums").leftJoin("Artists", "Albums.artist_id", "Artists.artist_id").then(albums => {
      if (!albums) {
        throw new Error("No albums found");
      }
      return _.sampleSize(albums, 8);
    }),
    Albums: (root, args) => knex.select("*").from("Albums").leftJoin("Genres", "Albums.genre_id", "Genres.genre_id").leftJoin("Artists", "Albums.artist_id", "Artists.artist_id").then(albums => {
      if (!albums) {
        throw new Error("No albums found");
      }
      return albums;
    }),

    Genres: (root, args) => knex.select("*").from("Genres").then(genres => {
      if (!genres) {
        throw new Error("No genres found");
      }
      return genres;
    }),
    Artists: (root, args) => knex.select("*").from("Artists").then(artists => {
      if (!artists) {
        throw new Error("No Artists found");
      }
      return artists;
    }),
    Playlists: (root, args) => knex.select("*").from("Playlists").then(playlists => {
      if (!playlists) {
        throw new Error("No Playlists found");
      }
      return playlists;
    }),
    Tracks: (root, args) => knex.select("*").from("Tracks").then(tracks => {
      console.log("selected", tracks);
      if (!tracks) {
        throw new Error("No Artists found");
      }
      return tracks;
    })
  }
};


/***/ }),
/* 7 */
/***/ (function(module, exports, __webpack_require__) {

/*** IMPORTS FROM imports-loader ***/
var graphql = __webpack_require__(0);

"use strict";

// This is needed in order to get the pg module bundled with webpack
// eslint-disable-next-line
const pg = __webpack_require__(8);
// eslint-disable-next-line
const mysql = __webpack_require__(9);

module.exports = {
  development: {
    client: "sqlite3",
    connection: {
      filename: "./dev.db"
    },
    useNullAsDefault: true
  },
  production: {
    client: "pg",
    connection: process.env.DATABASE_URL
  }
};


/***/ }),
/* 8 */
/***/ (function(module, exports) {

module.exports = require("pg");

/***/ }),
/* 9 */
/***/ (function(module, exports) {

module.exports = require("mysql");

/***/ }),
/* 10 */
/***/ (function(module, exports) {

module.exports = require("lodash");

/***/ }),
/* 11 */
/***/ (function(module, exports) {

module.exports = require("knex");

/***/ })
/******/ ])));