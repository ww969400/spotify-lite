exports.up = function(knex, Promise) {
  return knex.schema
    .createTable("Genres", table => {
      table.string("genre_id").primary();
      table.string("genre_name").notNull();
      table.timestamp("created_at").defaultTo(knex.fn.now());
    })
    .then(() => {
      return knex.schema
        .createTable("Artists", table => {
          table.string("artist_id").primary();
          table.string("artist_name").notNull();
          table.timestamp("created_at").defaultTo(knex.fn.now());
        })
        .then(() => {
          return knex.schema
            .createTable("Albums", table => {
              table.string("album_id").primary();
              table.string("album_name").notNull();
              table
                .enu("album_type", ["Full", "Ep"])
                .notNull()
                .defaultTo("Full");
              table.string("artist_id").notNull();
              table.string("genre_id").notNull();
              table.string("album_artwork").notNull();
              table.timestamp("created_at").defaultTo(knex.fn.now());
            })
            .then(() => {
              return knex.schema.createTable("Tracks", table => {
                table.string("track_id").primary();
                table.string("track_name").notNull();
                table.string("artist_id").notNull();
                table.string("genre_id").notNull();
                table
                  .boolean("is_single")
                  .notNull()
                  .defaultTo(false);
                table
                  .string("album_id")
                  .references("album_id")
                  .inTable("Albums")
                  .notNull()
                  .onDelete("cascade");
                table
                  .integer("track_plays")
                  .notNull()
                  .defaultTo(0);
                table.integer("album_order").notNull();
                table.string("track_path").notNull();
                table.string("track_duration").notNull();
                table.timestamp("created_at").defaultTo(knex.fn.now());
              });
            })
            .then(() => {
              return knex.schema
                .createTable("Playlists", table => {
                  table.increments("playlist_id");
                  table.string("playlist_name").notNull();
                  table.string("playlist_owner").notNull();
                  table.timestamp("created_at").defaultTo(knex.fn.now());
                })
                .then(() => {
                  return knex.schema.createTable("PlaylistSongs", table => {
                    table.increments("id");
                    table.integer("playlist_id").notNull();
                    table
                      .string("track_id")
                      .notNull()
                      .unique();
                    //table.integer("playlist_order").notNull();
                    table.timestamp("created_at").defaultTo(knex.fn.now());
                  });
                });
            });
        });
    });
};

exports.down = function(knex, Promise) {
  return Promise.all([
    knex.schema.dropTableIfExists("Genres"),
    knex.schema.dropTableIfExists("Artists"),
    knex.schema.dropTableIfExists("Albums"),
    knex.schema.dropTableIfExists("Tracks")
  ]);
};
